import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   Tnode() {}
   Tnode (String s, Tnode ns, Tnode fc) {
      name = s;
      firstChild = fc;
      nextSibling = ns;
   }
   public void setName(String s) {
      name = s;
   }

   public String getName() {
      return name;
   }

   public void setNextSibling(Tnode p) {
      nextSibling = p;
   }

   public Tnode getNextSibling() {
      return nextSibling;
   }

   public void setFirstChild(Tnode a) {
      firstChild = a;
   }

   public Tnode getFirstChild() {
      return firstChild;
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      Tnode child = getFirstChild();
      Tnode sibling = getNextSibling();
      b.append(name);
      if(child != null) b.append('(').append(child.toString());
      if(sibling != null) b.append(',').append(sibling.toString()).append(')');
      return b.toString();
   }

   public void addChildren(Tnode a) {
      if (a == null)
         throw new IllegalArgumentException("No node to add.");
      Tnode child = getFirstChild();
      if (child == null)
         setFirstChild(a);
      else if(child.getNextSibling() == null){
        child.setNextSibling(a);
      }
      else {
         child.addChildren(a);
      }
   }

   // 5 1 - 7 * 6 3 / +
   public static Tnode buildFromRPN(String pol) {
      Tnode tmp = null;
      String[] ss = pol.split("\\s+");
      if(pol.isEmpty())
         throw new IllegalArgumentException("Expression missing");
      List<String> lst = Arrays.asList("-", "+", "/", "*");
      List<String> actions = Arrays.asList("ROT", "SWAP");
      Stack<Tnode> stack = new Stack<>();
      for(String s : ss) {
         Tnode n = new Tnode();
         n.setName(s);
         if(actions.contains(s)) {
            if(stack.size() < 2)
               throw new IllegalArgumentException("too few elements: " + pol);
            Tnode first = stack.pop();
            Tnode second = stack.pop();
            if(s.equals("SWAP")) {
               stack.push(first);
               stack.push(second);
            }
            else {
               if(stack.size() < 3)
                  throw new IllegalArgumentException("too few elements: " + pol);
               Tnode third = stack.pop();
               stack.push(second);
               stack.push(first);
               stack.push(third);
            }
         }
         else {
            if(!lst.contains(s)) {
               try {
                  Integer.parseInt(s);
               } catch(Exception e){
                  throw new RuntimeException("Illegal input " + s + " in " + pol);
               }
            }
            else {
               try {
                  if(!stack.isEmpty()) {
                     tmp = stack.pop();
                  }
                  n.addChildren(stack.pop());
                  n.addChildren(tmp);
               } catch (Exception e){
                  throw new IllegalArgumentException("Too few numbers" + s + " in " + pol);
               }
            }
            stack.push(n);
         }
      }
      if(stack.size() > 1)
         throw new RuntimeException("Too many numbers " + pol);
      return stack.pop();
   }

   public static void main(String[] param) {
      String rpn = "5 1 - 7 * 6 3 / +";
      String rpn1 = "1 2 +";
      System.out.println("RPN: " + rpn);
      Tnode res = buildFromRPN(rpn);
      System.out.println("Tree: " + res.toString());
      String s1 = "2 5 SWAP -"; //- (5,2)
      String s2 = "2 5 9 ROT - +"; // + (5, - (9,2))
      String s3 = "2 5 9 ROT + SWAP -"; // - (+ (9,2), 5)
      System.out.print(buildFromRPN(s3).toString());
   }
}

